﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class CategoriaProductoViewModel
    {
        [Required]
        public int id { get; set; }

        [Required]
        public string nombre { get; set; }
    }
}
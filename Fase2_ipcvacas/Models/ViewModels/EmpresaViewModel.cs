﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class EmpresaViewModel
    {
        [Required]
        [StringLength(9)]
        [DisplayName("Nit")]
        public string nit { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre")]
        public string nombre { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "La {0} debe tener al menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [DisplayName("Contraseña")]
        public string pass { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Fecha de inicio")]
        public DateTime fecha_inico { get; set; }

        [Required]
        [DisplayName("Número de domicilio")]
        public int numero_domicilio { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class CiudadViewModel
    {
        [Required]
        public int Codigo { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public int CodigoDepartamento { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class AdminViewModel
    {
        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Pass { get; set; }
    }
}
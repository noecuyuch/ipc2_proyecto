﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class ProductoViewModel
    {
        [Required]
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre")]
        public string nombre { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Descripción")]
        public string descripcion { get; set; }

        [Required]
        [DisplayName("Imagen")]
        public string rutaImagen { get; set; }

        [Required]
        [DisplayName("Cantidad")]
        public int stock { get; set; }

        [Required]
        [DisplayName("Precio")]
        public decimal precio { get; set; }

        [Required]
        [DisplayName("Lista de precios")]
        public int idlistaPrecios { get; set; }

        [Required]
        [DisplayName("Categoria")]
        public int idcategoria { get; set; }

        public string nombreCategoria { get; set; }
    }
}
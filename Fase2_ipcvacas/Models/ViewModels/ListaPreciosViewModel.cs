﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class ListaPreciosViewModel
    {
        [Required]
        public int id { get; set; }

        [Required]
        [DisplayName("Fecha de inicio")] 
        public DateTime fecha_inicio { get; set; }

        [Required]
        [DisplayName("Fecha de vencimiento")]
        public DateTime fecha_final { get; set; }

        [Required]
        public bool esVigente { get; set; } //valor puede ser 0 o 1

        [Required]
        public int idTipoMoneda { get; set; }

        [Required]
        public List<ProductoViewModel> Productos{ get; set; } //recordar inicializar cuando se tiene un atributo lista

        public ListaPreciosViewModel()
        {
            Productos = new List<ProductoViewModel>();
        }
    }
}
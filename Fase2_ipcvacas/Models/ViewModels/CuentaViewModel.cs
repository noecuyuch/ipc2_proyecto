﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class CuentaViewModel
    {
        [Required(ErrorMessage = "Este campo es obligatorio")]
        [DisplayName("NIT")]
        public string nit { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [DisplayName("Contraseña")]
        [DataType(DataType.Password)]
        public string pass { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class MetaAnualViewModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public int Anio { get; set; }

        [Required]
        public decimal CantidadObjetivo { get; set; }

        [Required]
        public decimal CantidadActual { get; set; }

    }
}
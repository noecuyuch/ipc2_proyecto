﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class MetaMensualViewModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string Mes { get; set; }

        [Required]
        public decimal CantidadObjetivo { get; set; }

        [Required]
        public decimal CantidadActual { get; set; }

        [Required]
        public string NitEmpleado { get; set; }

        [Required]
        public int IDMetaAnual { get; set; }

    }
}
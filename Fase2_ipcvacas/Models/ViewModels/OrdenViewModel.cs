﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class OrdenViewModel
    {
        [Required]
        public int numeroOrden { get; set; }

        [Required]
        [Display(Name = "Total a pagar")]
        public decimal total { get; set; }  

        [Required]
        [Display(Name = "Fecha límite de pago")]
        public DateTime fechaLimite { get; set; }

        [Required]
        public string nit_Empleado { get; set; }

        [Required]
        [Display(Name = "Nit de cliente")]
        public string nit_cliente { get; set; }

        [Required]
        public int id_listaProductos { get; set; }

        [Required]
        public int id_listaOrdenes { get; set; }
    }
}
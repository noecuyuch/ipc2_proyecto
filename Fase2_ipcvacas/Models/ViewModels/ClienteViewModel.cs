﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

//modelo originalmente pensado solo para personas individuales, pero dado a que comparten los mismos atributos con Empresa se generalizó el modelo para ambos.

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class ClienteViewModel
    {
        [Required]
        [StringLength(9)]
        [DisplayName("Nit")]
        public string Nit { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombres")]
        public string Nombres { get; set; }

        [StringLength(50)]
        [DisplayName("Apellidos (Omitir si es empresa)")]
        public string Apellidos { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "La {0} debe tener al menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [DisplayName("Contraseña")]
        public string Pass { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Fecha de nacimiento/Inicio de operaciones")]
        public DateTime Fecha_nacimiento { get; set; }

        [DisplayName("Número de domicilio")]
        public int Numero_domicilio { get; set; }

        [Required]
        [DisplayName("Número de celular")]
        public int Numero_celular { get; set; }

        [Required]
        [StringLength(50)]
        [EmailAddress]
        [DisplayName("Correo electrónico")]
        public string Correo { get; set; }

        public string TipoCliente { get; set; }

        [Required]
        public int CodigoCiudad { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class EmpleadoViewModel
    {
        [Required]
        [StringLength(9)]
        [DisplayName("Nit")]
        public string nit { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombres")]
        public string nombres { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Apellidos")]
        public string apellidos { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "La {0} debe tener al menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [DisplayName("Contraseña")]
        public string pass { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Fecha de nacimiento")]
        public DateTime fecha_nacimiento { get; set; }

        [Required]
        [DisplayName("Numero de domicilio")]
        public int numero_domicilio { get; set; }

        [Required]
        [DisplayName("Número de celular")]
        public int numero_celular { get; set; }

        [Required]
        [StringLength(50)]
        [EmailAddress]
        [DisplayName("Email")]
        public string correo { get; set; }

        [Required]
        [StringLength(75)]
        [DisplayName("Dirección")]
        public string direccion { get; set; }

        [Required]
        [DisplayName("Puesto")]
        public string puesto { get; set; } //campo que indica el superior

        [DisplayName("Nit de supervisor")]
        public string nit_supervisor { get; set; }

        [DisplayName("Nit de gerente")]
        public string nit_gerente { get; set; }

        
    }
}
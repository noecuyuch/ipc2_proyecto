﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fase2_ipcvacas.Models.ViewModels
{
    public class DepartamentoViewModel
    {
        [Required]
        public int Codigo { get; set; }

        [Required]
        public string Nombre { get; set; }
    }
}
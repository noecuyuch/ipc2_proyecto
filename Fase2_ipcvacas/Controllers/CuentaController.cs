﻿using Fase2_ipcvacas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fase2_ipcvacas.Controllers
{
    public class CuentaController : Controller
    {
        string connectionString = @"Data Source=DESKTOP-UUIAD81; Initial Catalog= proyecto_ipc2beta4; Integrated Security=SSPI; MultipleActiveResultSets=true";

        public ActionResult Ingresar() //login
        {
            ViewBag.Title = "Ingreso";
            return View();
        }

        [HttpPost]
        public ActionResult Ingresar(CuentaViewModel datos) //login
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                
                sqlCon.Open();
                string sqlQuery = "select nit, pass, Puesto from Empleado where nit = @nit and pass = @pass";
                string queryAdmin = "select nombre, pass from Administrador where pass = @passAdmin";
                string queryCliente = "select nit, pass from Persona_Indiv where nit =@nitCliente and pass=@passCliente";
                string queryEmpresa = "select nit, pass from Empresa where nit = @nitEmpresa and pass = @passEmpresa";

                SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlCon);
                sqlCmd.Parameters.AddWithValue("@nit", datos.nit);
                sqlCmd.Parameters.AddWithValue("@pass", datos.pass);

                SqlCommand sqlCmdAdmin = new SqlCommand(queryAdmin, sqlCon);
                sqlCmdAdmin.Parameters.AddWithValue("@passAdmin", datos.pass);

                SqlCommand sqlCmdCliente = new SqlCommand(queryCliente, sqlCon);
                sqlCmdCliente.Parameters.AddWithValue("@nitCliente", datos.nit);
                sqlCmdCliente.Parameters.AddWithValue("@passCliente", datos.pass);

                SqlCommand sqlCmdEmpresa = new SqlCommand(queryEmpresa, sqlCon);
                sqlCmdEmpresa.Parameters.AddWithValue("@nitEmpresa", datos.nit);
                sqlCmdEmpresa.Parameters.AddWithValue("@passEmpresa", datos.pass);

                SqlDataReader sdrEmpleado = sqlCmd.ExecuteReader();
                SqlDataReader sdrAdmin = sqlCmdAdmin.ExecuteReader();
                SqlDataReader sdrCliente = sqlCmdCliente.ExecuteReader();
                SqlDataReader sdrEmpresa = sqlCmdEmpresa.ExecuteReader();

                if (sdrEmpleado.Read())
                {
                    Session["userNit"] = datos.nit.ToString();
                    Session["userPuesto"] = sdrEmpleado[2].ToString();

                    //if (sdr[2].ToString().Equals("Gerente"))
                    //{
                    //    return RedirectToAction("Index", "Empleado");
                    //}
                    //else if (sdr[2].ToString().Equals("Supervisor"))
                    //{

                    //}
                    return RedirectToAction("InicioEmpleado", "Empleado");
                }
                else if (sdrAdmin.Read())
                {
                    Session["userAdmin"] = datos.nit.ToString(); //se usará el campo nit para el nombre de usuario del admin
                    return RedirectToAction("IncioAdmin", "Admin");
                }
                else if (sdrCliente.Read())
                {
                    Session["userNit"] = datos.nit.ToString();

                    return RedirectToAction("InicioCliente", "Cliente");
                }
                else if (sdrEmpresa.Read())
                {
                    Session["userNit"] = datos.nit.ToString();

                    return RedirectToAction("InicioEmpresa", "Empresa");
                }
                else
                {
                    ViewData["Mensaje"] = "Datos incorrectos";
                }
                
                sqlCon.Close();
                return View();
                    
            }
        }

        public ActionResult Salir() //cerrar sesión
        {
            Session["userNit"] = null;
            Session["userPuesto"] = null;
            Session["userAdmin"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Registrar() //registrarse
        {
            return View();
        }
    }
}
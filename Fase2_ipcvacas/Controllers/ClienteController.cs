﻿using Fase2_ipcvacas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Fase2_ipcvacas.Controllers
{
    public class ClienteController : Controller
    {
        string connectionString = @"Data Source=DESKTOP-UUIAD81; Initial Catalog= proyecto_ipc2beta4; Integrated Security=SSPI; MultipleActiveResultSets=true";

        public ActionResult InicioCliente(ClienteViewModel cliente)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string query = "select * from Persona_Indiv where nit = @nit";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@nit", Session["userNit"].ToString());
                using (SqlDataReader sdr = sqlCmd.ExecuteReader())
                {
                    if (sdr.Read()) //en este caso se usa if porque solo se obtiene un resultado
                    {
                        cliente.Correo = sdr[6].ToString();
                    }
                }
                sqlCon.Close();
            }

            return View(cliente);
        }

        public ActionResult Index()
        {
            DataTable dbPersonas = new DataTable();
            DataTable dbEmpresas = new DataTable();

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string queryPersona = "SELECT * FROM Persona_Indiv where apellidos is not null";
                string queryEmpresa = "SELECT * FROM Persona_Indiv where apellidos is null";

                SqlDataAdapter sqlDaPersona = new SqlDataAdapter(queryPersona, sqlCon);
                sqlDaPersona.Fill(dbPersonas);

                SqlDataAdapter sqlDaEmpresa = new SqlDataAdapter(queryEmpresa, sqlCon);
                sqlDaEmpresa.Fill(dbEmpresas);
                sqlCon.Close();
            }

            Session.Add("TablaPersonas", dbPersonas);
            Session.Add("TablaEmpresas", dbEmpresas);

            return View();
        }

        [HttpPost]
        public ActionResult CargarClientes()
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(connectionString))
                {
                    if (Request.Files.Count > 0)
                    {
                        var xmlFile = Request.Files[0];

                        if (xmlFile != null && xmlFile.ContentLength > 0)
                        {
                            XmlDocument xmlDocument = new XmlDocument();
                            xmlDocument.Load(xmlFile.InputStream);
                            XmlNodeList ClienteNodes = xmlDocument.SelectNodes("definicion/cliente");
                            XmlNodeList DepaNodes = xmlDocument.SelectNodes("definicion/depto");
                            XmlNodeList CityNodes = xmlDocument.SelectNodes("definicion/ciudad");

                            foreach (XmlNode depa in DepaNodes)
                            {
                                sqlcon.Open();
                                string queryDepa = "insert into Departamento values(@codigo, @nombre)";
                                SqlCommand sqlcom = new SqlCommand(queryDepa, sqlcon);
                                sqlcom.Parameters.AddWithValue("@codigo", Convert.ToInt32(depa["codigo"].InnerText));
                                sqlcom.Parameters.AddWithValue("@nombre", depa["nombre"].InnerText);
                                sqlcom.ExecuteNonQuery();
                                sqlcon.Close();
                            }

                            foreach (XmlNode city in CityNodes)
                            {
                                sqlcon.Open();
                                string queryCity = "insert into Ciudad values(@codigo, @nombre, @codigoDepa)";
                                SqlCommand sqlcom = new SqlCommand(queryCity, sqlcon);
                                sqlcom.Parameters.AddWithValue("@codigo", Convert.ToInt32(city["codigo"].InnerText));
                                sqlcom.Parameters.AddWithValue("@nombre", city["nombre"].InnerText);
                                sqlcom.Parameters.AddWithValue("@codigoDepa", Convert.ToInt32(city["codigo_departamento"].InnerText));
                                sqlcom.ExecuteNonQuery();
                                sqlcon.Close();
                            }

                            foreach (XmlNode cliente in ClienteNodes)
                            {

                                sqlcon.Open();
                                string queryCliente = "insert into Persona_Indiv values(@nit, @nombres, @apellidos, '12345', @fecha, @numDom, @numCel, @correo, @cod_ciudad, @direccion)";
                                
                                SqlCommand sqlcom = new SqlCommand(queryCliente, sqlcon);
                                sqlcom.Parameters.AddWithValue("@nit", cliente["NIT"].InnerText);
                                sqlcom.Parameters.AddWithValue("@nombres", cliente["nombres"].InnerText);
                                sqlcom.Parameters.AddWithValue("@apellidos", cliente["apellidos"].InnerText);
                                sqlcom.Parameters.AddWithValue("@fecha", Convert.ToDateTime(cliente["nacimiento"].InnerText));
                                sqlcom.Parameters.AddWithValue("@numDom", Convert.ToInt32(cliente["telefono"].InnerText));
                                sqlcom.Parameters.AddWithValue("@numCel", Convert.ToInt32(cliente["celular"].InnerText));
                                sqlcom.Parameters.AddWithValue("@correo", cliente["email"].InnerText);
                                sqlcom.Parameters.AddWithValue("@cod_ciudad", Convert.ToInt32(cliente["ciudad"].InnerText));
                                sqlcom.Parameters.AddWithValue("@direccion", cliente["direccion"].InnerText);
                                sqlcom.ExecuteNonQuery();
                                sqlcon.Close();

                                sqlcon.Open();
                                string queryCredito = "insert into Credito values(@limite, @restante, @nit, @dias)";
                                SqlCommand sqlcomCredito = new SqlCommand(queryCredito, sqlcon);
                                sqlcomCredito.Parameters.AddWithValue("@limite", Convert.ToDecimal(cliente["limite_credito"].InnerText));
                                sqlcomCredito.Parameters.AddWithValue("@restante", Convert.ToDecimal(cliente["limite_credito"].InnerText));
                                sqlcomCredito.Parameters.AddWithValue("@nit", cliente["NIT"].InnerText);
                                sqlcomCredito.Parameters.AddWithValue("@dias", Convert.ToInt32(cliente["dias_credito"].InnerText));
                                sqlcomCredito.ExecuteNonQuery();
                                sqlcon.Close();
                            }

                        }
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
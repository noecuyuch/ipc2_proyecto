﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using Fase2_ipcvacas.Models.ViewModels;
using System.Xml;

namespace Fase2_ipcvacas.Controllers
{
    public class EmpleadoController : Controller
    {
        // conexión con base de datos
        string connectionString = @"Data Source=DESKTOP-UUIAD81; Initial Catalog= proyecto_ipc2beta4; Integrated Security=SSPI"; //SSPI por autenticación de Windows

        public ActionResult InicioEmpleado(EmpleadoViewModel empleado)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string query = "select * from Empleado where nit = @nit";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@nit", Session["userNit"].ToString());
                using (SqlDataReader sdr = sqlCmd.ExecuteReader())
                {
                    if (sdr.Read()) //en este caso se usa if porque solo se obtiene un resultado
                    {
                        empleado.correo = sdr[7].ToString();
                    }
                }
                sqlCon.Close();
            }

            return View(empleado);
        }

        [HttpGet]
        public ActionResult Index()
        {

            DataTable dbEmpleados = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM Empleado", sqlCon);
                sqlDa.Fill(dbEmpleados);
                sqlCon.Close();
            }

            return View(dbEmpleados);
        }


        // Enviando a la vista un nuevo objeto del modelo de Empleados
        [HttpGet]
        public ActionResult Create()
        {
            return View(new EmpleadoViewModel());
        }

        [HttpPost]
        public ActionResult Create(EmpleadoViewModel empleado)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                string nit_supervisor = empleado.nit_supervisor;
                string nit_gerente = empleado.nit_gerente;
                string puesto_empleado = empleado.puesto;

                sqlCon.Open();
                string query = "insert into Empleado values(@nit, @nombres, @apellidos, @pass, @fecha_nacimiento, @numero_domicilio, @numero_celular, @correo, @direccion, @nit_supervisor, @nit_gerente, @Puesto)";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@nit", empleado.nit);
                sqlCmd.Parameters.AddWithValue("@nombres", empleado.nombres);
                sqlCmd.Parameters.AddWithValue("@apellidos", empleado.apellidos);
                sqlCmd.Parameters.AddWithValue("@pass", empleado.pass);
                sqlCmd.Parameters.AddWithValue("@fecha_nacimiento", empleado.fecha_nacimiento);
                sqlCmd.Parameters.AddWithValue("@numero_domicilio", empleado.numero_domicilio);
                sqlCmd.Parameters.AddWithValue("@numero_celular", empleado.numero_celular);
                sqlCmd.Parameters.AddWithValue("@correo", empleado.correo);
                sqlCmd.Parameters.AddWithValue("@direccion", empleado.direccion);

                if (empleado.puesto == "Gerente")
                {
                    sqlCmd.Parameters.AddWithValue("@nit_supervisor", DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@nit_gerente", DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@Puesto", "Gerente");
                }
                else if (empleado.puesto == "Supervisor")
                {
                    sqlCmd.Parameters.AddWithValue("@nit_supervisor", DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@nit_gerente", empleado.nit_gerente);
                    sqlCmd.Parameters.AddWithValue("@Puesto", "Supervisor");
                }
                else if (empleado.puesto == "Vendedor")
                {
                    sqlCmd.Parameters.AddWithValue("@nit_supervisor", empleado.nit_supervisor);
                    sqlCmd.Parameters.AddWithValue("@nit_gerente", DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@Puesto", "Vendedor");
                }
                sqlCmd.ExecuteNonQuery();
                sqlCon.Close();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CargarEmpleados()
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(connectionString))
                {
                    if (Request.Files.Count > 0)
                    {
                        var xmlFile = Request.Files[0];

                        if (xmlFile != null && xmlFile.ContentLength > 0)
                        {
                            XmlDocument xmlDocument = new XmlDocument();
                            xmlDocument.Load(xmlFile.InputStream);
                            XmlNodeList EmpleadoNodes = xmlDocument.SelectNodes("definicion/empleado");

                            foreach (XmlNode empleado in EmpleadoNodes)
                            {
                                sqlcon.Open();
                                string query = "insert into Empleado values(@nit, @nombres, @apellidos, @pass, @fecha, @numDom, @numCel, @correo, @direccion, @nit_supervisor, @nit_gerente, @Puesto)";
                                SqlCommand sqlcom = new SqlCommand(query, sqlcon);
                                sqlcom.Parameters.AddWithValue("@nit", empleado["NIT"].InnerText);
                                sqlcom.Parameters.AddWithValue("@nombres", empleado["nombres"].InnerText);
                                sqlcom.Parameters.AddWithValue("@apellidos", empleado["apellidos"].InnerText);
                                sqlcom.Parameters.AddWithValue("@pass", empleado["pass"].InnerText);
                                sqlcom.Parameters.AddWithValue("@fecha", Convert.ToDateTime(empleado["nacimiento"].InnerText));
                                sqlcom.Parameters.AddWithValue("@numDom", Convert.ToInt32(empleado["telefono"].InnerText));
                                sqlcom.Parameters.AddWithValue("@numCel", Convert.ToInt32(empleado["celular"].InnerText));
                                sqlcom.Parameters.AddWithValue("@correo", empleado["email"].InnerText);
                                sqlcom.Parameters.AddWithValue("@direccion", empleado["direccion"].InnerText);

                                //si es un gerente
                                if (Convert.ToInt32(empleado["codigo_puesto"].InnerText) == 3)
                                {
                                    sqlcom.Parameters.AddWithValue("@nit_supervisor", DBNull.Value);
                                    sqlcom.Parameters.AddWithValue("@nit_gerente", DBNull.Value);
                                    sqlcom.Parameters.AddWithValue("@Puesto", "Gerente");
                                }
                                else if (Convert.ToInt32(empleado["codigo_puesto"].InnerText) == 2)
                                {
                                    sqlcom.Parameters.AddWithValue("@nit_supervisor", DBNull.Value);
                                    sqlcom.Parameters.AddWithValue("@nit_gerente", empleado["codigo_jefe"].InnerText);
                                    sqlcom.Parameters.AddWithValue("@Puesto", "Supervisor");
                                }
                                else if (Convert.ToInt32(empleado["codigo_puesto"].InnerText) == 1)
                                {
                                    sqlcom.Parameters.AddWithValue("@nit_supervisor", empleado["codigo_jefe"].InnerText);
                                    sqlcom.Parameters.AddWithValue("@nit_gerente", DBNull.Value);
                                    sqlcom.Parameters.AddWithValue("@Puesto", "Vendedor");
                                }
                                sqlcom.ExecuteNonQuery();
                            }

                        }
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

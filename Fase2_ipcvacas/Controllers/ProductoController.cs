﻿using Fase2_ipcvacas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml;

namespace Fase2_ipcvacas.Controllers
{
    public class ProductoController : Controller
    {
        string connectionString = @"Data Source=DESKTOP-UUIAD81; Initial Catalog= proyecto_ipc2beta4; Integrated Security=SSPI; MultipleActiveResultSets=true";

        public ActionResult Index()
        {
            DataTable dbProductos = new DataTable();
            DataTable dbListasPrecios = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();

                string query = "select Producto.codigo, Producto.nombre, Producto.descripcion, Producto.id_listaPrecios, " +
                    "CategoriaProducto.nombre_categoria from Producto inner join CategoriaProducto on Producto.id_categoria = CategoriaProducto.id";

                string queryListasPrecios = "select ListaPrecios.id, CAST(ListaPrecios.fecha_inicio as date), " +
                    "CAST(ListaPrecios.fecha_vencimiento as date), ListaPrecios.es_vigente from ListaPrecios";

                SqlDataAdapter sqlDa = new SqlDataAdapter(query, sqlCon);
                SqlDataAdapter sqlDaListasPrecios = new SqlDataAdapter(queryListasPrecios, sqlCon);
                sqlDa.Fill(dbProductos);
                sqlDaListasPrecios.Fill(dbListasPrecios);

                sqlCon.Close();

                Session.Add("ListasPrecios", dbListasPrecios);
            }

            return View(dbProductos);
        }

        public ActionResult HabilitarLista(int idlista)
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                sqlcon.Open();
                string query = "update ListaPrecios set es_vigente = 'True' where id = @idlista;";
                SqlCommand sqlcom = new SqlCommand(query, sqlcon);
                sqlcom.Parameters.AddWithValue("@idlista", idlista);
                sqlcom.ExecuteNonQuery();
                sqlcon.Close();
            }

            return RedirectToAction("Index");
        }

        public ActionResult DeshabilitarLista(int idlista)
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                sqlcon.Open();
                string query = "update ListaPrecios set es_vigente = 'False' where id = @idlista;";
                SqlCommand sqlcom = new SqlCommand(query, sqlcon);
                sqlcom.Parameters.AddWithValue("@idlista", idlista);
                sqlcom.ExecuteNonQuery();
                sqlcon.Close();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CargarProductos()
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(connectionString))
                {
                    if (Request.Files.Count > 0)
                    {
                        var xmlFile = Request.Files[0];

                        if (xmlFile != null && xmlFile.ContentLength > 0)
                        {
                            XmlDocument xmlDocument = new XmlDocument();
                            xmlDocument.Load(xmlFile.InputStream);
                            XmlNodeList ProductoNodes = xmlDocument.SelectNodes("definicion/producto");
                            XmlNodeList CategoriaNodes = xmlDocument.SelectNodes("definicion/categoria");

                            foreach (XmlNode categoria in CategoriaNodes)
                            {
                                sqlcon.Open();
                                string query = "insert into CategoriaProducto values(@codigo, @categoria)";
                                SqlCommand sqlcom = new SqlCommand(query, sqlcon);
                                sqlcom.Parameters.AddWithValue("@codigo", Convert.ToInt32(categoria["codigo"].InnerText));
                                sqlcom.Parameters.AddWithValue("@categoria", categoria["nombre"].InnerText);
                                sqlcom.ExecuteNonQuery();
                                sqlcon.Close();
                            }

                            foreach (XmlNode producto in ProductoNodes)
                            {
                                sqlcon.Open();
                                string queryProducto = "insert into Producto values(@codigo, @nombre, @descripcion, '', 0, @idlista, @idcategoria)";
                                SqlCommand sqlcomProducto = new SqlCommand(queryProducto, sqlcon);
                                sqlcomProducto.Parameters.AddWithValue("@codigo", Convert.ToInt32(producto["codigo"].InnerText));
                                sqlcomProducto.Parameters.AddWithValue("@nombre", producto["nombre"].InnerText);
                                sqlcomProducto.Parameters.AddWithValue("@descripcion", producto["descripcion"].InnerText);
                                sqlcomProducto.Parameters.AddWithValue("@idlista", DBNull.Value);
                                sqlcomProducto.Parameters.AddWithValue("@idcategoria", Convert.ToInt32(producto["categoria"].InnerText));
                                sqlcomProducto.ExecuteNonQuery();
                                sqlcon.Close();
                            }
                        }
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult CargarListas()
        {

            return View();
        }
    }
}
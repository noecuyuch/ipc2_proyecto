﻿using Fase2_ipcvacas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fase2_ipcvacas.Controllers
{
    public class AdminController : Controller
    {
        string connectionString = @"Data Source=DESKTOP-UUIAD81; Initial Catalog= proyecto_ipc2beta4; Integrated Security=SSPI";

        public ActionResult IncioAdmin(AdminViewModel admin)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string query = "select * from Administrador where nombre = @name";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@name", Session["userAdmin"] as string);
                using (SqlDataReader sdr = sqlCmd.ExecuteReader())
                {
                    if (sdr.Read()) //en este caso se usa if porque solo se obtiene un resultado
                    {
                        admin.Nombre = sdr[1].ToString();
                    }
                }
                sqlCon.Close();
            }

            return View(admin);
        }
    }
}